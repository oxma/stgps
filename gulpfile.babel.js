import gulp from 'gulp';
import { Pug, PugLinter }   from './gulp/tasks/pug';
import { Style, StyleLinter, StyleVendor }  from './gulp/tasks/style';
import { Serve }    from './gulp/tasks/server';
import { WatchFiles }   from './gulp/tasks/watch';
import { Script, ScriptLinter, ScriptVendor }   from './gulp/tasks/script';
import { Images }   from './gulp/tasks/image';
import { Fonts }    from './gulp/tasks/font';
import { Screenshots } from './gulp/tasks/screenshot'

const build = gulp.series (
    Images,
    Fonts,
    gulp.series (
        ScriptLinter,
        ScriptVendor,
        Script
    ),
    gulp.series (
        StyleLinter,
        StyleVendor,
        Style
    ),
    gulp.series (
        PugLinter,
        Pug
    ),
);

gulp.task('build', build);

export default build;