export const path = {
    pug: {
        src: [
            './src/pages/*.pug',
            './src/pages/**/*.pug',
        ],
        dist: './public',
        watch: [
            './src/layouts/*.pug',
            './src/pages/*.pug',
            './src/pages/**/*.pug',
            './src/components/*.pug',
            './src/components/**/*.pug',
            './src/sections/*.pug',
            './src/sections/**/*.pug',
        ],
        linter: [
            './src/layouts/*.pug',
            './src/pages/*.pug',
            './src/pages/**/*.pug',
            './src/components/*.pug',
            './src/components/**/*.pug',
            './src/sections/*.pug',
            './src/sections/**/*.pug',
        ]
    },

    style: {
        src: './src/assets/scss/style.scss',
        dist: './public/assets/css/',
        watch: [
            './src/assets/scss/*.scss',
            './src/pages/**/*.scss',
            './src/sections/**/*.scss',
            './src/components/**/*.scss'
        ],
        linter: [
            './src/assets/scss/*.scss',
            './src/pages/**/*.scss',
            './src/sections/**/*.scss',
            './src/components/**/*.scss'
        ]
    },

    styleVendor: {
        src: './src/assets/scss/vendor.scss',
        dist: './public/assets/css/',
        watch: './src/assets/scss/vendor.scss'
    },

    script: {
        src: './src/assets/js/script.js',
        dist: './public/assets/js/',
        watch: [
            './src/assets/js/*.js',
            './src/sections/**/*.js'
        ],
        linter: [
            './src/sections/**/*.js'
        ]
    },

    scriptVendor: {
        src: './src/assets/js/vendor.js',
        dist: './public/assets/js/',
        watch: './src/assets/js/*.js'
    },

    img: {
        src: [
            '!./src/assets/img/icon/*.*',
            './src/assets/img/**/*.*'
        ],
        dist: './public/assets/img/',
        watch: [
            '!./src/assets/img/icon/*.*',
            './src/assets/img/**/*.*'
        ]
    },

    fonts: {
        src: './src/assets/fonts/**/*.*',
        dist: './public/assets/fonts/',
        watch: './src/assets/fonts/**/*.*',
    },

    deploy: {
        src: './public/**',
        root: './public/',
        destination: 'yousite/public_html/',
        exclude: [
            '**/Thumbs.db', 
            '**/*.DS_Store',
            '.idea/'
        ]
    }
};