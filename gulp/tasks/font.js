import gulp from 'gulp';
import { path } from '../path';

export const Fonts = () => {
    return gulp.src(path.fonts.src)
        .pipe(gulp.dest(path.fonts.dist))
}
