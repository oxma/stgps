import gulp         from 'gulp';
import { path }     from '../path';
import browserSync  from 'browser-sync';

export function Serve(done) {
    browserSync({
        server: {
            baseDir: path.pug.dist,
            index: 'index.html'
        },
        ghostMode: true,
        notify: false,
        host: '0.0.0.0',
        port: 9000
    });

    done();
};