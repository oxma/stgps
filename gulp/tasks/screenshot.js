import gulp                 from 'gulp';
import { path }             from '../path';
import webshot              from 'gulp-webshot';

export const Screenshots = () => {
    return gulp.src('./build/*.html')
        .pipe(webshot({
            dest:'screenshots/',
            root:'build'
        }))
};