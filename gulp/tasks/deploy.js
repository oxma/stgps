import gulp from 'gulp';
import { path } from '../path';
import { deploy } from 'deploy.config.js';

export const Fonts = () => {
    return gulp.src(path.deploy.src)
        .pipe(rsync({
            root: path.deploy.root,
            hostname: deploy.username+'@'+deploy.site,
            destination: path.deploy.destination,
            // include: ['*.htaccess'], // Includes files to deploy
            exclude:  path.deploy.exclude, // Excludes files from deploy
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }))
}