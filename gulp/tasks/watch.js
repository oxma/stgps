import gulp                 from 'gulp';
import { 
    Pug,
    PugLinter
}                           from './pug';
import { 
    StyleLinter,
    Style,
    StyleVendor 
}                           from './style';
import { 
    ScriptLinter,
    Script,
    ScriptVendor
}                           from './script';
import { Images }           from './image';
import { path }             from '../path';

export const WatchFiles = () =>
    gulp.watch(path.pug.watch, gulp.parallel(PugLinter, Pug));
    gulp.watch(path.style.watch, gulp.parallel(StyleLinter, Style, StyleVendor));
    gulp.watch(path.script.watch, gulp.parallel(Script, ScriptVendor, ScriptLinter));
    gulp.watch(path.img.watch, gulp.parallel(Images));
