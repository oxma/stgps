import gulp             from 'gulp';
import babel            from 'gulp-babel';
import sourcemaps       from 'gulp-sourcemaps';
import concat           from 'gulp-concat';
import changed          from 'gulp-changed';
import minify           from 'gulp-minify';
import browserSync      from 'browser-sync';
import eslint           from 'gulp-eslint';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';
import fileInclude      from 'gulp-file-include';
import { path }         from '../path';

export const Script = () => {
    return gulp.src(path.script.src)
        .pipe(changed(path.script.dist))
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sourcemaps.init())
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(babel({
            "presets": ["@babel/preset-env"]
        }))
        .pipe(concat('script.min.js'))
        .pipe(minify({
            ext:{
                min:'.js'
            },
            ignoreFiles: ['.combo.js', '.min.js'],
            noSource: true
        }))
        .pipe(sourcemaps.write("../maps"))
        .pipe(gulp.dest(path.script.dist))
        .pipe(browserSync.stream());
};

export const ScriptVendor = () => {
    return gulp.src(path.scriptVendor.src)
        .pipe(changed(path.script.dist))
        .pipe(babel())
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(concat('vendor.min.js'))
        .pipe(minify({
            ext:{
                min:'.js'
            },
            noSource: true
        }))
        .pipe(gulp.dest(path.scriptVendor.dist));
};

export const ScriptLinter = () => {
    return gulp.src(path.script.linter)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
        .pipe(browserSync.stream());
};