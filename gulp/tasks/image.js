import gulp             from 'gulp';
import { path }         from '../path';
import imagemin         from 'gulp-imagemin';
import changed          from 'gulp-changed';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';
import browserSync      from 'browser-sync';

export const Images = () => {
    return gulp.src(path.img.src)
        .pipe(changed(path.img.dist, {
            extension: '*.*'
        }))
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(path.img.dist))
        .pipe(browserSync.reload({ stream: true }));
};