import gulp             from 'gulp';
import { path }         from '../path';
import pug              from 'gulp-pug';
import pugLinter        from 'gulp-pug-linter';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';
import browserSync      from 'browser-sync';

export const Pug = () => {
    return gulp.src(path.pug.src)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(pug({
            pretty: '    '
        }))
        .pipe(gulp.dest(path.pug.dist))
        .pipe(browserSync.stream())
};

export const PugLinter = () => {
    const pugErrors = (errors) => {
        errors.map(error => console.error(error.message));
    };
    return gulp.src(path.pug.linter)
        .pipe(pugLinter({
            reporter: pugErrors
        }))
};