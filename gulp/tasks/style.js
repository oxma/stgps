import gulp                 from 'gulp';
import scss                 from 'gulp-sass';
import postcss              from 'gulp-postcss';
import postcssScss          from 'postcss-scss';
import postcssReporter      from 'postcss-reporter';
import autoprefixer         from 'gulp-autoprefixer';
import stylelint            from 'stylelint';
import sourcemaps           from 'gulp-sourcemaps';
import concat               from 'gulp-concat';
import cleanCSS             from 'gulp-clean-css';
import changed              from 'gulp-changed';
import plumber              from 'gulp-plumber';
import notify               from 'gulp-notify';
import browserSync          from 'browser-sync';
import { path }             from '../path';

export const Style = () => {
    return gulp.src(path.style.src)
        .pipe(changed(path.style.dist))
        .pipe(sourcemaps.init())
        .pipe(scss())
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest(path.style.dist))
        .pipe(browserSync.stream());
};

export const StyleLinter = () => {
    return gulp.src(path.style.linter)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(postcss([
            stylelint(),
            postcssReporter({
                clearReportedMessages: true,
                throwError: false,
            }),
        ], {
            syntax: postcssScss
        }))
        .pipe(browserSync.stream());
};

export const StyleVendor = () => {
    return gulp.src(path.styleVendor.src)
        .pipe(changed(path.styleVendor.dist))
        .pipe(sourcemaps.init())
        .pipe(scss())
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(path.styleVendor.dist))
        .pipe(sourcemaps.write('../maps'))
        .pipe(browserSync.stream());
};